//
// Created by naman on 02/03/22.
//
// 2) Read a string and count how many times 'A' appears in it

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100];
    scanf("%s", string1);
    int i = 0, count=0;
    while (string1[i] != '\0'){
        if (string1[i] == 'A')
            count++;
        i++;
    }
    printf("Letter 'A' appears %d times in given string.", count);
}