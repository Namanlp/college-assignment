//
// Created by naman on 02/03/22.
//
//  1) Read a string using %s and find its length using loop.

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100];
    scanf("%s", string1);
    int i = 0, length=0;
    while (string1[i] != '\0'){
        length++;
        i++;
    }
    printf("Length of string is : %d", length);
}