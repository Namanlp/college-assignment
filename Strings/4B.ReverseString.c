//
// Created by naman on 03/03/22.
//
//4) Using loop, reverse a string into (i) a new string (ii) into itself without using another string.

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100];
    scanf("%s", string1);
    int  length=0;
    while (string1[length] != '\0')
        length++;

    for (int i=0; i< length/2;i++){
        char temp = string1[i];
        string1[i]= string1[length-i-1];
        string1[length-i-1] = temp;
    }
    printf("Reverse string is : %s ", string1);
}