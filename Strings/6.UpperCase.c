//
// Created by naman on 04/03/22.
//
// 6) From a string, count number of upper-case letters in it.

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100];
    scanf("%s", string1);
    int  i=0, upper=0;
    while (string1[i] != '\0'){
        if (string1[i] >= 'A' && string1[i] <='Z')
            upper++;
        i++;
    }
    printf("There are %d uppercase letters in string %s", upper, string1);
}