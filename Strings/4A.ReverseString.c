//
// Created by naman on 03/03/22.
//
//4) Using loop, reverse a string into (i) a new string (ii) into itself without using another string.

#include <stdio.h>

int main(){
    printf("Enter the string : ");
    char string1[100], string2[100];
    scanf("%s", string1);
    int  length=0;
    while (string1[length] != '\0')
        length++;

    for (int i=0; i< length;i++){
        string2[i]= string1[length-i-1];
    }
    printf("Reverse string is : %s ", string2);
}