//
// Created by naman on 04/03/22.
//
// 5) Concatenate two strings into a new string. Concatenated string must be stored and then displayed using %s

#include <stdio.h>

int main(){
    char string1[100], string2[100];
    printf("Enter the first string : ");
    scanf("%s", string1);
    printf("Enter second string : ");
    scanf(" %s", string2);
    int  length1=0, length2=0, i=0;
    while (string1[length1] != '\0')
        length1++;
    while (string2[length2] != '\0')
        length2++;
    char string3[length1 + length2 +2];
    for (; i<length1; i++) {
        string3[i] = string1[i];
    }
    string3[i++] = ' ';
    for (int j=0; j < length2; j++){
        string3[i] = string2[j];
        i++;
    }
    string3[length1+length2+1] = '\0';
    printf("Concatenated string is : %s", string3);
}