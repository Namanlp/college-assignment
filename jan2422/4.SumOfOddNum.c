//
// Created by naman on 28/01/22.
//
// 4)  Find Sum of n odd nos.


#include <stdio.h>

int main() {
    int totalNum = 0, sum=1, num= 1;
    printf("How many odd numbers to find sum : ");
    scanf("%d", &totalNum);
    for (int i =1; i < totalNum; ++i) {
        num +=2;
        sum += num;
    }
    printf("Sum of %d odd numbers is %d", totalNum, sum);
}
