//
// Created by naman on 28/01/22.
//
//10) **Find whether a given number is prime or not.

#include <stdio.h>
#include <math.h>

int main(){
    int originalNum = 0, isPrime = 1, num=2;
    printf("Enter number to check : ");
    scanf("%d", &originalNum);
    double squareRoot = sqrt( originalNum);

    do {
        if (originalNum%num == 0)
            isPrime = 0;
        else
            num++;
    } while ( isPrime  && num<= (int)squareRoot);
    if (isPrime || originalNum == 2)
        printf("Yes, %d is a Prime number", originalNum);
    else
        printf("No, %d number is not a prime number.\nIt is divisible by %d", originalNum, num);
}