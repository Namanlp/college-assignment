//
// Created by naman on 28/01/22.
//
//7) Using do-while, keep on reading numbers and find their sum till number entered is not -1.

#include <stdio.h>

int main(){
    int sum = 0, num =0;
    do {
        printf("Enter number to add (-1) to stop : ");
        scanf("%d", &num);
        sum+= num;
    } while (num != -1);

    printf("Sum is : %d", sum);

}