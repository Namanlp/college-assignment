//
// Created by naman on 30/01/22.
//
//8) Using do-while, implement above series sum

#include <stdio.h>

int main(){
    int totalNum = 0, fact=1, multiple=1;
    printf("Enter number to find factorial : ");
    scanf("%d", &totalNum);
    do {
        fact*= multiple;
        multiple++;
    } while (multiple != totalNum+1);
    printf("Factorial of %d is %d", totalNum, fact);
}