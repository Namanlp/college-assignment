//
// Created by naman on 28/01/22.
//
// 9) Find ASCII code of a symbol (say $), of alphabet A, of alphabet a, of digit 0 etc.

#include <stdio.h>
int main() {
    char character;
    printf("Enter a character: ");
    scanf("%c", &character);
    printf("ASCII value of %c = %d", character, character);
    return 0;
}