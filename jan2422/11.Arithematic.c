//
// Created by naman on 29/01/22.
//
//11`) Using if-else, implement calculator operations for supporting choices for four
//arithmetic operations +,-, *, / Program should continue repeating the calculations of
//the operations till Stop choice is not given.

#include <stdio.h>

int main(){
    int choice= 0;
    printf("Hi there, welcome to the program. ");

    do {
        printf("Enter Choice\n1 => Addition \n2 => Subtraction \n3=> Multiplication \n4 => Division \n5=> Quit\n");
        scanf("%d", &choice);

        if (choice >= 1 && choice <= 4){
            int num1= 0, num2=0;
            printf("Enter first number : ");
            scanf("%d", &num1);
            printf("Enter second number : ");
            scanf("%d", &num2);

         if (choice==1)
             printf("Addition of numbers, %d and %d gives %d", num1, num2, num1+num2);
         else if (choice == 2)
             printf("Subtracting  %d from %d gives %d", num2, num1, num1-num2);
         else if (choice == 3)
             printf("Multiplication of numbers, %d and %d gives %d", num1, num2, num1*num2);
         else if (choice == 4)
             printf("Dividing  %d by %d gives %d", num2, num1, num1/num2);
        } else if (choice == 5)
            printf("Thanks! Come Again!");
        else
            printf("Sorry, invalid choice, try again!");

        printf("\n");
    } while (choice != 5);
}