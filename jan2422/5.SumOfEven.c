//
// Created by naman on 28/01/22.
//
// 5) Sum of even numbers up to n (n being even).


#include <stdio.h>

int main() {
    int uptoNum = 0, sum=0;
    printf("Enter any even number  : ");
    scanf("%d", &uptoNum);
    if (uptoNum%2 == 0){
        int num = 0;
        while (num!= uptoNum){
            sum += num+2;
            num += 2;
        }
        printf("Sum of even numbers upto %d is %d", uptoNum, sum);
    } else
        printf("Sorry, given number is not Even");
}