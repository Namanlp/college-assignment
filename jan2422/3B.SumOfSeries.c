//
// Created by naman on 24/01/22.
//

#include <stdio.h>

int main() {
    int num = 0, sum=0, i =1, temp=1;
    printf("Enter limit to find sum : ");
    scanf("%d", &num);

    // 1+2+3... N times
    for (sum = temp ;temp<num;){
        printf("%d + ", temp);
        temp++;
        sum+= temp;
    }
    printf("%d = %d\n", temp, sum);

    // 5+7+9+... N times
    temp = 5;
    sum = temp;
    for (;temp+2<num;sum+=temp){
        printf("%d + ", temp);
        temp+= 2;
    }
    printf("%d = %d\n", temp, sum);

    //    1+5+10+15 ... N times
    temp = 5, sum=1, i=1;
    printf("1 + ");
    for (    temp = 5, sum=1, i=1; temp<num; i++){
        printf("%d + ", temp);
        sum+= temp;
        temp+= 5;
    }
    sum+= temp;
    printf("%d = %d\n", temp, sum);

//    2+4+8+... N times
    temp = 2;
    sum = temp;
    for (;temp*2<num;){
        printf("%d + ", temp);
        temp*= 2;
        sum+= temp;
    }
    printf("%d = %d\n", temp, sum);

//    S=1 -3 +5 -7 ... N times

    temp = 3, i=1;
    sum = 1;
    printf("%d", sum);
    for (;temp<num;i++){
        if (i%2!=0){
            printf(" - %d", temp);
            sum -= temp;
        }
        else{
            printf(" + %d", temp);
            sum += temp;
        }
        temp+=2;
    }
    printf("= %d\n", sum);

}