//
// Created by naman on 29/01/22.
//
// 12)Using loop, find sum of digits of a given number. Number will be read as integer

#include <stdio.h>

int main() {
    int number = 0, sum=0;
    printf("Enter number to find sum of digits : ");
    scanf("%d", &number);

    while (number != 0){
        sum += number%10;
        number/=10;
    }
    printf("Sum of digits is : %d", sum);

}