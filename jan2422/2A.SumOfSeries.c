//
// Created by naman on 24/01/22.
//
// Using while, find sum of series:

#include <stdio.h>

int main() {
    int num = 0, sum=0, i =1, temp=1;
    printf("Enter number of times to find sum : ");
    scanf("%d", &num);

    // 1+2+3... N times
    sum = temp;
    while (temp<num){
        printf("%d + ", temp);
        temp++;
        sum+= temp;
    }
    printf("%d = %d\n", temp, sum);

    // 5+7+9+... N times
    temp = 5;
    sum = temp;
    while (i<num){
        printf("%d + ", temp);
        temp+= 2;
        i++;
        sum+= temp;
    }
    printf("%d = %d\n", temp, sum);

    //    1+5+10+15 ... N times
    temp = 5, sum=1, i=1;
    printf("1 + ");
    while (i<num-1){
        printf("%d + ", temp);
        sum+= temp;
        temp+= 5;
        i++;
    }
    sum+= temp;
    printf("%d = %d\n", temp, sum);


//    2+4+8+... N times
    temp = 2, i=1;
    sum = temp;
    while (i<num){
        printf("%d + ", temp);
        temp*= 2;
        i++;
        sum+= temp;
    }
    printf("%d = %d\n", temp, sum);

//    S=1 -3 +5 -7 ... N times

    temp = 3, i=1;
    sum = 1;
    printf("%d", sum);
    while (i<num){
        if (i%2!=0){
            printf(" - %d", temp);
            sum -= temp;
        }
        else{
            printf(" + %d", temp);
            sum += temp;
        }
        temp+=2;
        i++;
    }
    printf("= %d\n", sum);

}