//
// Created by naman on 10/01/22.
// Question :  Print grades of a student A+/A/B/C/D/E using if-else ladder
//

#include <stdio.h>

int main(){
    int marks=0;
    printf("Enter Marks percentage : ");
    scanf("%d", &marks);
    if (marks > 90)
        printf("You got an A+ Grade!");
    else if (marks > 80)
        printf("You got an A Grade!");
    else if (marks > 70)
        printf("You got a B Grade!");
    else if (marks > 60)
        printf("You got a C Grade!");
    else if (marks > 50)
        printf("You got a D Grade!");
    else if (marks > 40)
        printf("You got an E Grade!");
    else
        printf("Sorry, you failed. Work harder to pass the exam.");
}