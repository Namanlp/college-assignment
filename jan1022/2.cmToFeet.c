//
// Created by Naman on 10/01/22.
//

// Question : Length is given in centimeters. Convert it into feet and inches.

#include <stdio.h>

int main(){

    float length;
    printf("Enter length in cm : ");
    scanf("%f", &length);
    printf("\nLength in feet is %f feet", length/30.48);
    printf("\nLength in inches is %f inches", length/2.54);

    return 0;
}