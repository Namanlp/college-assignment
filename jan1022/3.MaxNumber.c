//
// Created by Naman on 10/01/22.
//

// Question : Using if-else and && operator, find max out of 3 distinct numbers

#include <stdio.h>

int main(){
    printf("Enter 3 numbers : ");
    int num1 =0 , num2 =0 , num3 = 0;
    scanf("%d %d %d", &num1, &num2 , &num3);

    int  maxNumber;
    if (num1 > num2 && num1 > num3)
        maxNumber = num1;
    else if (num2 > num3 && num2 > num1)
        maxNumber = num2;
    else
        maxNumber = num3;

    printf("Maximum number is : %d", maxNumber);
}