//
// Created by naman on 10/01/22.
//

// Question : Without using && etc. find max as well as min out of 3 numbers. Same "if" should be used to find max as well as min. Aim to use minimum number of if statements.

#include <stdio.h>

int main(){
    printf("Enter 3 numbers : ");
    int num1 =0 , num2 =0 , num3 = 0;
    scanf("%d %d %d", &num1, &num2 , &num3);

    int  maxNumber = num1;
    int minNumber = num1;
    if (num2 > maxNumber)
        maxNumber = num2;
    else minNumber = num2;

    if (num3 > maxNumber)
        maxNumber = num3;
    else if (num3 < minNumber) minNumber = num3;

    printf("Maximum number is : %d", maxNumber);
    printf("\nMinimum number is : %d" , minNumber);
    return 0;
}