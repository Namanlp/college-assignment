//
// Created by naman on 10/01/22.
//
// Question : Find max out of 4 distinct numbers without using && etc
//

#include <stdio.h>

int main(){
    printf("Enter 4 numbers : ");
    int num1 =0 , num2 =0 , num3 = 0, num4 = 0;
    scanf("%d %d %d %d", &num1, &num2 , &num3, &num4);

    int  maxNumber = num1;
    if (num2 > maxNumber)
        maxNumber = num2;
    if (num3 > maxNumber)
        maxNumber = num3;
    if (num4 > maxNumber)
        maxNumber = num4;

    printf("Maximum number is : %d", maxNumber);

    return 0;
}