//
// Created by naman on 10/01/22.
//
// Question : Find roots of quadratic equation. Roots can be real/equal/complex.

#include <stdio.h>
#include <math.h>

int main(){
    printf("Enter coefficients of x^2 , x and constant term : ");
    int a=0, b = 0, c= 0;
    scanf("%d %d %d", &a, &b, &c);
    double D =(b*b) - (4 * a * c);


    if (D >= 0){
        printf( "Roots are : %2f and %2f" , (sqrt(D) - b)/(4*a),  (-1*sqrt(D) - b)/(4*a) );
    } else{
        D*= -1;
        printf("Roots are : %2f + %2fi and %2f - %2fi", -1*b/(4.0*a), sqrt(D)/(4.0*a), -1*b/(4.0*a), sqrt(D)/(4.0*a)  );
    }
}