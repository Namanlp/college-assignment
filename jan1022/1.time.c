//
// Created by Naman on 10/01/22.
//

// Question -> Convert time given in 24-hour format into 12-hour format.

#include <stdio.h>

int main(){
    printf("Input hours and minutes separated by space in HH:MM format. For example, 23 07 for 23:07. Input : ");
    unsigned int hours=0, minutes =0;
    scanf("%d %d", &hours, &minutes);

    if ( hours <= 23 && minutes <= 59){
        unsigned int hours12 = hours %13;
        if (hours > 12)
            hours12+=1;
        printf("Time in 12-hour format is %d:", hours12);
        if (minutes<=9)
            printf("0%d", minutes);
        else
            printf("%d", minutes);
        if (hours < 12){
            printf(" am");
        } else{
            printf(" pm");}
    } else
        printf("\nSorry, invalid input.");

    return  0;
}