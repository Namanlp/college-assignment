//
// Created by naman on 10/01/22.
//
// Question : Using if-else, find max out of 3 distinct numbers. Use of && etc. is not allowed

#include <stdio.h>

int main(){
    printf("Enter 3 numbers : ");
    int num1 =0 , num2 =0 , num3 = 0;
    scanf("%d %d %d", &num1, &num2 , &num3);

    int  maxNumber = num1;
    if (num2 > maxNumber)
        maxNumber = num2;
    if (num3 > maxNumber)
        maxNumber = num3;

    printf("Maximum number is : %d", maxNumber);

    return 0;
}