//
// Created by naman on 02/02/22.
//
//3) Using loops, find sum of digits of a given number, which is of float
//type. (Hint: integer part of the float number can be separated by assigning
//it to an int; fractional part can also be separated; find sum of digits of
//both parts separately)

#include <stdio.h>
int main()
{
    float originalNum = 0;

    printf("Enter the number:\n");
    scanf("%f",&originalNum);
    int nonDecimal =  ((int) originalNum) * 1000 ,   a = 0, sum1 = 0, sum2 = 0;
    int Decimal = (int) (originalNum * 1000) - ((int) originalNum) * 1000;
    while(nonDecimal>0){
        a=nonDecimal % 10;
        nonDecimal=nonDecimal/10;
        sum1=sum1+a;
    }

    while(Decimal>0){
        a=Decimal % 10;
        Decimal=Decimal/10;
        sum2=sum2+a;
    }
    printf("The sum of non decimal digits  = %d" , sum1);
    printf("\nThe sum of 3 decimal digits  = %d" , sum2);
    return 0;
}