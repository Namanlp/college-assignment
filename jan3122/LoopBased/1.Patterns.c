//
// Created by naman on 31/01/22.
//
//1) Print the following patterns:-


#include <stdio.h>

int main() {
    printf("Enter number of lines to print : ");
    int totalLines = 0;
    scanf("%d",
          &totalLines);
    //I)
    //*
    //**
    //***
    //****

    for (int i = 1;
         i <=
         totalLines; i++) {
        for (int j = 1;
             j <=
             i; ++j) {
            printf("*");
        }
        printf("\n");
    }

    printf("\n");
    //ii)
    //   *
    //  **
    // ***
    //****
    for (int i = 1; i <= totalLines; i++) {
        for (int j = 1;j <=(totalLines-i); ++j) {
            printf(" ");
        }
        for (int j = 1;j <=i; ++j) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");

//    iii)
//   *
//  ***
// *****
//*******
    for (int i = 1; i <= totalLines; i++) {
        for (int j = 1;j <=(totalLines-i); ++j) {
            printf(" ");
        }
        for (int j = 1;j <=2*i-1; ++j) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");

    // In reverse
    for (int i = 1; i <= totalLines; i++) {
        for (int j = 1;j <=i-1; ++j) {
            printf(" " );
        }
        for (int j = 1;j <=2*(totalLines-i)+1; ++j) {
            printf("*");
        }
        printf("\n");
    }
    printf("\n");

    //iv)
    //  A
    // ABC
    //ABCDE

    if(totalLines<=13){
        for (int i = 1; i <= totalLines; i++) {
            for (int j = 1;j <=(totalLines-i); ++j) {
                printf(" ");
            }
            for (int j = 1;j <=2*i-1; ++j) {
                printf("%c", j+64);
            }
            printf("\n");
        }
        printf("\n");
    } else
        printf("Sorry, not this pattern");

    //V)
    //  1
    // 121
    //12321

    for (int i = 1; i <= totalLines; i++) {
        for (int j = 1;j <=(totalLines-i); ++j) {
            printf(" ");
        }
        for (int j = 1;j <=i; ++j) {
            printf("%d", j%10);
        }
        for (int j = 1;j <i; ++j) {
            printf("%d", (i-j)%10);
        }
        printf("\n");
    }
    printf("\n");

}
