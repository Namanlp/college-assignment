//
// Created by naman on 01/02/22.
//
//2) Using loop, find sum of digits of a given number. Number will be read as integer.

#include <stdio.h>

int main(){
    printf("Enter number : ");
    int number=0, sum=0;
    scanf("%d", &number);
    while (number!=0){
        sum+= number%10;
        number/=10;
    }
    printf("Sum of digits is %d", sum);
}