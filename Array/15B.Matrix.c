//
// Created by naman on 21/02/22.
//
//15) Find and store transpose of a non-square matrix (i) into a new matrix, (ii) into itself
//without using any extra array/matrix. Display the result as row-wise matrix.

#include <stdio.h>

int main(){
    int Matrix[3][3] ={
            {1, 5, 6 },
            {4, 4, 8},
            {2, 3, 4}};

    printf("Old Matrix\n");
    for (int row = 0;row<3;row++){
        for (int column = 0;column <3; column++){
            printf("\t%d \t", Matrix[row][column]);
        }
        printf("\n");
    }
    for (int row = 1;row<3;row++){
        for (int column = 0;column <2; column++){
            int tempo = Matrix[row][column] ;
            Matrix[row][column] = Matrix[column][row];
            Matrix[column][row] = tempo;
        }}

    printf("New Matrix\n");
    for (int row = 0;row<3;row++){
        for (int column = 0;column <3; column++){
            printf("\t%d \t", Matrix[row][column]);
        }
        printf("\n");
    }
}