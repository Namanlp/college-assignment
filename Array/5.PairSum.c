//
// Created by naman on 21/02/22.
//
//5) Input an array and find pair wise sum i.e. sum of first two values, next two value and so
//on. If n is odd, double the last value.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }

    for (int i =0; i<num;i+=2){
        if (i+1==num){
            printf("\nSum of pair is (double last number because odd number of elements) : %d" , 2*array1[i]);
        } else
            printf("\nSum of pair is : %d ", array1[i]+array1[i+1]);
    }
}