//
// Created by naman on 22/02/22.
//
//14) Assume a class of N students having roll numbers 0..N-1. Their P, C, M marks are to be
//inputted. Use one 2-d array While reading the input, give suitable message for each input like
//"Enter Chemistry Marks of Roll No 5". Now compute and store in


#include <stdio.h>
int main()
{
    int no;
    printf("Enter the number of students :\n");
    scanf("%d",&no);
    int marks[no+2][4];
    for(int i=0;i<no+2;i++)
    {
        for (int j=0;j<4;j++) {
            marks[i][j] = 0;
        }
    }
    for(int i=0;i<no;i++)
    {
        printf("Enter the marks of Chemistry of Roll No. %d :  ",i);
        scanf(" %d",&marks[i][0]);
        printf("Enter the marks of Physics of Roll No. %d :  ",i);
        scanf(" %d",&marks[i][1]);
        printf("Enter the marks of Maths of Roll No. %d :  ",i);
        scanf(" %d",&marks[i][2]);
        marks[i][3]=marks[i][0]+marks[i][1]+marks[i][2];
        printf("The total marks of roll no %d in all subjects :  %d\n ",i,marks[i][3]);
        printf("----------------------------------------------------------------------------\n");
    }
    for (int j = 0; j<4; j++){
        for (int i=0; i<no; i++){
            marks[no+1][j] += marks[i][j];
        }
        marks[no+1][j] = (int)((float )marks[no+1][j]/(float)no);
    }

    printf("\nAverage in Chemistry : %d \nAverage in Physics : %d \nAverage in Maths : %d \nAverage marks : %d ", marks[no+1][0], marks[no+1][1], marks[no+1][2], marks[no+1][3]);
    printf("----------------------------------------------------------------------------\n");
    return 0;
}