//
// Created by naman on 17/02/22.
//
// 3) Count number of –ive numbers, no of even numbers, no of odd numbers in an array.


#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }

    int negative=0, even=0, odd=0;

    for (int i=0; i<num; i++){
        if (array1[i]<0)
            negative++;
        if (array1[i]%2==0)
            even++;
        else
            odd++;
    }
    printf("Number of Negative numbers : %d \nNumber of Even numbers : %d \nNumber of Odd numbers : %d ", negative, even, odd);
}