//
// Created by naman on 22/02/22.
//
//13) Store the addition of two matrices into a new matrix, if they are addition-compatible.
//Display it row-wise on the screen.

#include <stdio.h>

int main(){
    int Matrix[3][3] ={
            {5, 9, 6 },
            {1, -2, 7},
            {2, 4, 0}};
    int NewMatrix[3][3];

    for (int row = 0;row<3;row++){
        for (int column = 0;column <3; column++){
            NewMatrix[row][column] = Matrix[column][row];
        }}
    printf("Old Matrix\n");
    for (int row = 0;row<3;row++){
        for (int column = 0;column <3; column++){
            printf("\t%d \t", Matrix[row][column]);
        }
        printf("\n");
    }
    printf("New Matrix\n");
    for (int row = 0;row<3;row++){
        for (int column = 0;column <3; column++){
            printf("\t%d \t", NewMatrix[row][column]);
        }
        printf("\n");
    }
}