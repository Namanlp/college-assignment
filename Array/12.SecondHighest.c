//
// Created by naman on 19/02/22.
//
//12) Find second highest element of an array without sorting it.

#include <stdio.h>

int main(){

    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    int largestNum=array1[0], secondLargestNum = array1[0];

    for (int number =0 ; number< num; number++){
        if (array1[number]>secondLargestNum){
            secondLargestNum=array1[number];
            if  (array1[number]>largestNum){
                secondLargestNum=largestNum;
                largestNum = array1[number];
            }
        }
    }
    printf("Largest Number is : %d\nSecond Largest Number is : %d", largestNum, secondLargestNum);
}