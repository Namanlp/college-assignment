//
// Created by naman on 15/02/22.
//
// 1) Find average of element in an array.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }

    float sum=0;
    for (int i = 0; i<num; i++){
        sum += (float)array1[i];
    }
    printf("Sum of values : %3f\nAverage of Values : %3f ", sum, sum/(float)num);
}