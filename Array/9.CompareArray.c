//
// Created by naman on 21/02/22.
//
// 9) Compare whether two arrays are equal or not
#include <stdio.h>

int main(){
    int num1 = 0;
    printf("Please Enter Number of  elements in first array : ");
    scanf("%d", &num1);
    int array1[num1];
    printf("Enter Array : ");
    for (int i = 0;i < num1; ++i) {
        scanf(" %d", &array1[i]);
    }
    int num2 = 0;
    printf("Please Enter Number of elements in second array : ");
    scanf("%d", &num2);
    int array2[num2];
    printf("Enter Array : ");
    for (int i = 0;i < num2; ++i) {
        scanf(" %d", &array2[i]);
    }
    int isEqual=1;
    if (num1!=num2)
        isEqual=0;

    for (int i=0;i <num1 && isEqual;i++){
        if (array1[i] != array2[i])
            isEqual=0;
    }
    if (isEqual){
        printf("Yes, arrays are equal");
    } else
        printf("No, arrays are not equal");
}