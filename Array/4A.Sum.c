//
// Created by naman on 21/02/22.
//
//4) Find sum of odd positioned elements of an array, also find sum of even positioned
//elements of the array (i) using two separate loops (ii) using same loop.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);

    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    int sumEven=0, sumOdd=0;
    for(int i = 0 ; i<num; i+=2){
        sumEven+= array1[i];
    }
    for(int i = 1 ; i<num; i+=2){
        sumOdd+= array1[i];
    }
    printf("Sum of Even positioned numbers are : %d\nSum of Odd positioned numbers are : %d", sumEven, sumOdd);
}