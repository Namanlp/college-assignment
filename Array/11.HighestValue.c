//
// Created by naman on 22/02/22.
//
//11) From an array, find the highest value, find compute how many values it appears, how
//many values are lesser than it, how many are greater than it.

#include <stdio.h>

int main(){
    int num = 0;
    printf("Please Enter Number of elements : ");
    scanf("%d", &num);
    int array1[num];
    printf("Enter Array : ");
    for (int i = 0;i < num; ++i) {
        scanf(" %d", &array1[i]);
    }
    int highestVal=array1[0], count=1;
    for (int i=1;i<num;i++){
        if (array1[i]>highestVal){
            highestVal=array1[i];
            count = 1;
        } else if (array1[i]==highestVal)
            count++;
    }
    printf("Highest value is %d and it appears %d times in given array.\nThere are %d values lesser than it 0 values greater than it.", highestVal, count, num-count);
}