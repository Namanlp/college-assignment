//
// Created by naman on 07/03/22.
//
// Given array p[5] , shift circularly left by 2 positions.
// Given 4x5 matrix, shift rows left by 2

// 4 rows, 5 columns, first make function, then perform function for each row.

void rotateLeft( int p[5] ) {
    int copyArray[5];
    for (int i = 0; i <5;i++)
        copyArray[i] = p[i];

    for (int i=0; i<5;i++){
        // Take new variable, j , to prevent negative index of array p
        int j=i-2;
        if (j < 0)
            j+=5;
        p[j] = copyArray[i];
    }
}

#include <stdio.h>

int main(){
    int givenMatrix[4][5];
    for (int i=0; i<4; i++){
        for (int j=0; j<5; j++){
            printf("Enter %d row %d element : ", i, j);
            scanf("%d", &givenMatrix[i][j]);
        }
    }


    // Printing Given Matrix

    printf("Matrix entered is \n");
    for (int i=0; i<4;i++){
        for (int j=0; j<5; j++)
            printf(" %d ", givenMatrix[i][j]);
        printf("\n");
        }
    // Applying Function to each row of matrix

    for (int i=0; i<4; i++)
        rotateLeft(givenMatrix[i]);

    // Printing New Matrix

    printf("New Matrix is \n");
    for (int i=0; i<4;i++){
        for (int j=0; j<5; j++)
            printf(" %d ", givenMatrix[i][j]);
        printf("\n");
    }

}

