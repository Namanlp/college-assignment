//
// Created by naman on 09/03/22.
//
// 2. For a class of n students (roll number 1...N), store their physics, chem, math marks in 3 separate 1
//-dimensional arrays. Find the total marks scored by the student having highest marks in (i) physics (ii)
//chem (iii) math. Report if same student has highest marks in more than one subject. Assume that
//there are no duplicity in highest marks of a subject. Note: - solve this question using function.


int findMax( int * list, int length){
    int largestIndex = 1;
    for (int i = 1; i <= length; i++){
        if (list[i] > list[largestIndex])
            largestIndex = i;
    }
    list[0] = largestIndex;
    return largestIndex;

}

#include <stdio.h>

int main(){
    int students = 0;
    printf("Please enter the number of the students : ");
    scanf(" %d", &students);

    int physics[students+1], maths[students+1], chemistry[students+1];
    for(int student=1;student<=students;student++){
        printf("\n========================================================\n");
        printf("Enter the marks of Physics of Roll No. %d :  ",student);
        scanf(" %d",&physics[student]);
        printf("Enter the marks of Chemistry of Roll No. %d :  ",student);
        scanf(" %d",&chemistry[student]);
        printf("Enter the marks of Maths of Roll No. %d :  ",student);
        scanf(" %d",&maths[student]);
        }

    printf("\nHighest marks in Physics is achieved by Roll number %d , marks = %d", findMax(physics, students), physics[findMax(physics, students)]);
    printf("\nHighest marks in Chemistry is achieved by Roll number %d , marks = %d", findMax(chemistry, students), chemistry[findMax(chemistry, students)] );
    printf("\nHighest marks in Mathematics is achieved by Roll number %d , marks = %d", findMax(maths, students), maths[findMax(maths, students)]);

    if( physics[0] == chemistry[0] || physics[0] == maths[0] )
        printf(" \nYes student with roll number %d has highest marks in more than one subject", physics[0]);
    else if (maths[0] == chemistry [0])
        printf(" \nYes student with roll number %d has highest marks in more than one subject", maths[0]);

}