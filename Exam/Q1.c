//
// Created by naman on 09/03/22.
//
//Write a program to check given String is palindrome or not.

#include <stdio.h>

int main(){
    char givenString[101] = " " ;
    printf("Please enter the string (upto 100 characters ) : ");
    scanf(" %s", givenString);
    int length=0, palindrome = 1;
    for (int i=0; givenString[i] != '\0'; i++ )
        length++;
    printf("Length of given string is : %d", length);

    for (int i=0; i<= length/2 ; i++  )
        if ( givenString[i] != givenString [ length - i - 1]){
            palindrome = 0;
            break;
        }

    if (palindrome)
        printf("\nYes, %s is a palindrome", givenString);
    else
        printf("\nNo, %s is not a palindrome", givenString);


}