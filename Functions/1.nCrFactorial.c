//
// Created by naman on 06/03/22.
//
// 1) Using function for factorial with suitable parameters & return type, compute n Cr.

#include <stdio.h>

int factorial(int num){
    int product = 1;
    for (int i=1; i<=num;i++)
        product*=i;
    return product;
}

int combinations(int n, int r){
    if (r>n){
        printf("Sorry, wrong input. r can not be greater than n");
        return 0;
    }
    return factorial(n)/ (factorial(r) * factorial(n-r));
}

int main(){
    printf("Please enter the value of n and r : ");
    int r =0, n=0;
    scanf("%d %d", &n, &r);
    printf("Result of %dC%d is = %d ", n, r, combinations( n,r));
}