//
// Created by naman on 08/03/22.
//
// 10) Write a function to find average of an array of float values. Write another function to
//count number of values above the average. Also find all pairs of two consecutive values such
//that sum of the pair is less than the average.

#include <stdio.h>

double findAverage(const float * givenArray, int length){
    double sum = 0;
    for (int i=0; i < length; i++)
        sum += givenArray[i];
    return sum/length;
}

int findAboveAverage( const float  * givenArray, int length, double average){
    int count = 0;
    for (int i = 0 ; i< length; i++)
        if(givenArray[i] > average)
            count++;
    return count;
}

int findBelowAveragePair( const float  * givenArray, int length, double average){
    int count = 0;
    for (int i = 0 ; i< length-1; i++)
        if(givenArray[i] + givenArray[i+1] < average)
            count++;
    return count;
}

int main(){
    int length = 0;
    printf("Enter the length of array : ");
    scanf(" %d", &length);
    float givenArray[length];
    printf("Enter Array : ");
    for (int i= 0; i < length; i++)
        scanf(" %f", &givenArray[i]);

    double average = findAverage( givenArray , length);

    printf("\nAverage of entered Array is : %f\nNumber of Values above average are : %d\nNumber of consecutive pairs whose sum is less than Average are %d ",
           average, findAboveAverage(givenArray, length, average) , findBelowAveragePair(givenArray, length, average));
}