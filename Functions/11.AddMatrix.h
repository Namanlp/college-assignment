//
// Created by naman on 08/03/22.
//

#ifndef FUNCTIONS_11_ADDMATRIX_H

#define FUNCTIONS_11_ADDMATRIX_H

#include <stdio.h>

int **readMatrix(){
    int rows = 0, columns =0;
    printf("\nEnter the number of rows : ");
    scanf(" %d", &rows);
    printf("Enter number of columns : ");
    scanf(" %d", &columns);
    int matrix[rows][columns];

    for (int i = 0; i < rows; i++){
        printf("Enter row number %d : ", i);
        for (int j=0; j < columns; j++)
            scanf(" %d", &matrix[i][j]);
    }
}

void displayMatrix(const int * givenMatrix[], int rows, int columns){
    for (int i=0; i < rows; i++)
        for (int j=0; j<columns; j++)
            printf(" %d ", givenMatrix[i][j]);
}

#endif //FUNCTIONS_11_ADDMATRIX_H
