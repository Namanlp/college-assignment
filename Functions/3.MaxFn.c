//
// Created by naman on 06/03/22.
//

int maxFn(int a, int b, int c){
    if (a>=b && a>=c)
        return a;
    else if (b >= a && b>=c)
        return b;
    else
        return c;
}

#include <stdio.h>

int main(){
    int arrayNum[9];
    printf("Please enter array of 9 elements : ");
    for (int i =0; i < 9; i++)
        scanf(" %d", &arrayNum[i]);

    int maxNum = maxFn(
            maxFn(arrayNum[0], arrayNum[1], arrayNum[2]),
            maxFn(arrayNum[3], arrayNum[4], arrayNum[5]),
            maxFn(arrayNum[6], arrayNum[7], arrayNum[8])
    );
    printf("Maximum number in the given array is : %d", maxNum);
}