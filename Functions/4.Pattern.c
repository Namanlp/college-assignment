//
// Created by naman on 07/03/22.
//
//4) Using function, display pattern of above Q 10 (c), where the pattern should be printed
//using * or ? or $ or any other symbol. symbol to be used will be input from the user and
//should be parameter to the function. N will also be another (obviously) parameter to the
//function.

#include <stdio.h>

void pattern(char symbol , int totalLines){

    for (int i=1; i <= totalLines; i++){
        for (int j = 1; j<=(totalLines-i); j++)
            printf(" ");
        for (int j=1; j<= 2*i-1; j++)
            printf("%c",symbol);
        printf("\n");
    }

    for (int i=2; i <= totalLines; i++){
        for (int j = 1; j<=i-1; j++)
            printf(" ");
        for (int j=1; j<= 2*(totalLines-i)+1; j++)
            printf("%c",symbol);
        printf("\n");
    }
}

int main() {
    printf("Enter character to print : ");
    char symbol;
    scanf(" %c", &symbol);
    printf("Enter number of lines to print : ");
    int totalLines = 0;
    scanf("%d", &totalLines);

    pattern(symbol, totalLines);
}