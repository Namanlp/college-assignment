//
// Created by naman on 08/03/22.
//
// 5) Using function series_sum, find sum of series : S= x - x3 /31 +x5 /51 - x7 /71 ... upto xn
///n!. Implement it in two ways: (i) series_sum function internally calls fact and pow function
//(ii) series_sum function does not use pow & fact functions and each term is generated from
//previous term iteratively.

#include <stdio.h>

int fact(int n){
    int factorial = 1;
    for (int i=1; i<=n; i++)
        factorial *=i;
    return factorial;
}

int pow2(int x, int n){
    for (int i = 1; i< n; i++)
        x *= x;
    return x;
}

double series_sum1( int x, int number ){
    double sum = 0;
    for (int index=1; index <= number; index++){
        sum += pow2(-1, 5) * (double) pow2( x, 2*index - 1)  / fact(2*index - 1);
        printf(" %f\n", pow2(-1, index) * (double) pow2( x, 2*index - 1)  / fact(2*index - 1));
    }
    return  sum;
}

int main(){
    int x = 0, numberTerms = 0;
    printf("Enter the value of x : ");
    scanf(" %d", &x);
    printf("Enter the number of terms : ");
    scanf(" %d", &numberTerms);

    printf("Sum of series is : %f", series_sum1(x, numberTerms));

}