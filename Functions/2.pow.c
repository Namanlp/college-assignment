//
// Created by naman on 06/03/22.
//

#include <stdio.h>

int pow(int a , int b){
    int product =1;
    for (int i =1; i<=b; i++)
        product*=a;
    return product;
}

int main(){
    printf("Please enter a and b : " );
    int a=0, b=0;
    scanf("%d %d", &a, &b);
    printf("Result of %d raise to power %d = %d ", a, b, pow(a,b));
}