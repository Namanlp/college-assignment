//
// Created by naman on 13/03/22.
//
// 8) For 1-dimesional array, Write functions for each of the following: (i) average (ii) max, (iii)
//min (iv) second highest, assuming highest value is not duplicated. Array reading will be done
//in main.

#include <stdio.h>

double findAverage(const int * givenArray, int length){
    double sum = 0;
    for (int i=0; i < length; i++)
        sum += givenArray[i];
    return sum/length;
}
int findMax(const int * givenArray, int length){
    int max  = givenArray[0];
    for (int i=0; i < length; i++)
        if (givenArray[i] > max)
            max = givenArray[i];
    return max;
}
int findMin(const int * givenArray, int length){
    int min  = givenArray[0];
    for (int i=0; i < length; i++)
        if (givenArray[i] < min)
            min = givenArray[i];
    return min;
}

int secondHighest(const int * givenArray, int length){
    int highest= givenArray[0], secondHighest = givenArray[0];
    for (int i=0; i < length; i++)
        if (givenArray[i] > secondHighest){
            secondHighest = givenArray[i];
            if (secondHighest > highest){
                secondHighest = highest;
                highest = givenArray[i];
            }
        }
    return secondHighest;
}

int main(){
    int length = 0;
    printf("Enter the length of array : ");
    scanf(" %d", &length);
    int givenArray[length];
    printf("Enter Array : ");
    for (int i= 0; i < length; i++)
        scanf(" %d", &givenArray[i]);

    printf("\nAverage value is : %f", findAverage( givenArray , length));
    printf("\nMaximum value is : %d", findMax(givenArray, length));
    printf("\nSecond highest value is : %d", secondHighest(givenArray, length));
    printf("\nMinimum value is : %d", findMin(givenArray, length));

}