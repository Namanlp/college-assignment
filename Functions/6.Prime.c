//
// Created by naman on 08/03/22.
//
// 6) Write a function to check whether a number is prime or not, find first N prime
//numbers starting from 2 onwards

#include <stdio.h>

int isPrime(int num){
    int isPrime = 1;
    for (int i=2; i * i <= num && isPrime ; i++ ){
        if (num % i == 0)
            isPrime = 0;
    }
    return isPrime;
}

int main(){
    int number = 0, primes = 0;
    printf("Enter the number of terms : ");
    scanf("%d", &number);

    for (int i =2; i<= number; i++ )
        if (isPrime(i))
            primes++;

    printf("Number of Prime numbers from 2 to %d are : %d", number, primes);
}