//
// Created by naman on 05/02/22.
//
//3) Read a date as collection of 3 integers (d,m,y). Using switch-case, find
//number of remaining days in that month. For example 15 Aug 2019
//means, 16 days remaining. Implement it using (i) calculating number of
//remaining days in all 12 different cases (ii) think of doing these
//calculations min time i.e. clubbing together same calculation cases and
//calculating once only for the clubbed cases

#include <stdio.h>

int main() {
    printf("Enter Date DD MM YYYY  :  ");
    int date=0, month=0, year = 0;
    scanf("%d %d %d",  &date, &month, &year);

    //calculating number of remaining days in all 12 different cases

    switch (month) {
        case 1 :    printf(" Current month is January, and %d are remaining in this month", 31-date);   break;
        case 2 :    printf(" Current month is February, ");
        if (year%4==0)
            printf("and %d are remaining in this month", 29-date);
        else
                printf("and %d are remaining in this month", 28-date);
        break;
        case 3 :    printf(" Current month is March, and %d are remaining in this month", 31-date);   break;
        case 4 :    printf(" Current month is April, and %d are remaining in this month", 30-date);   break;
        case 5 :    printf(" Current month is May, and %d are remaining in this month", 31-date);   break;
        case 6 :    printf(" Current month is June, and %d are remaining in this month", 30-date);   break;
        case 7 :    printf(" Current month is July, and %d are remaining in this month", 31-date);   break;
        case 8 :    printf(" Current month is August, and %d are remaining in this month", 31-date);   break;
        case 9 :    printf(" Current month is September, and %d are remaining in this month", 30-date);   break;
        case 10 :    printf(" Current month is October, and %d are remaining in this month", 31-date);   break;
        case 11 :    printf(" Current month is November, and %d are remaining in this month", 30-date);   break;
        case 12:    printf(" Current month is December, and %d are remaining in this month", 31-date);   break;
        default:   printf("Sorry, invalid month number"); break;
    }

}