//
// Created by naman on 03/02/22.
//
// 1) Given a day number, print day of the week assuming 1 means
//Monday... 7 meaning
//Sunday

#include <stdio.h>

int main(){
    printf("Enter Day Between 1 to 7 : ");
    int num=0;
    scanf("%d", &num);
    printf("Today is ");

    switch (num) {
        case 1 :    printf("Monday"); break;
        case 2 :    printf("Tuesday"); break;
        case 3 :    printf("Wednesday"); break;
        case 4 :    printf("Thursday"); break;
        case 5 :    printf("Friday"); break;
        case 6 :    printf("Saturday"); break;
        case 7 :    printf("Sunday"); break;
        default:    printf("Invalid Choice!");
    }

}