//
// Created by naman on 12/02/22.
//
//4) Implement simple calculator using loops and switch. Previously
//calculated can get used in next repetition. Or user may reset it to 0, by
//pressing C(for clear).Calculator should keep on working till user does not
//decide to Exit.

#include <stdio.h>

int main(){
    int running =1, num1=0, num2=0, result=0;
    char choice;
    while (running){
        if (num1 == 0){
            printf("\n Enter first number : ");
            scanf(" %d", &num1);
        } else
            printf("\nFirst number is %d, Choose option C to clear\n" , num1);
        printf("Enter Second number : ");
        scanf(" %d", &num2);

        printf("\n\ta-> Addition \n\ts->Subtraction \n\tm->Multiplication \n\td->Division \n\tc->Clear \n\t q->Quit \nEnter Choice : ");
        scanf(" %c", &choice);
        switch (choice) {
            case 'A' :case 'a' : result = num1 + num2;     break;
            case 'S': case 's' : result = num1 - num2;     break;
            case 'm' : case 'M' : result = num1 * num2;     break;
            case 'd' : case 'D' : result = num1 / num2;     break;
            case 'c' : case 'C' : result = num1 = num2=0; break;
            case 'q' : case 'Q': printf("Thank You. Exiting program"); running=0; break;
            default: printf("Sorry, invalid operator!");
        }
        printf("\nResult is : %d", result);
        num1 = result;
        result =0;

        printf("\nDo you want to exit ? (Y/N)");
        scanf(" %c", &choice);
        if (choice == 'Y' || choice == 'y') {
            printf("Thank You. Exiting program"); running=0;
        }
    }

}